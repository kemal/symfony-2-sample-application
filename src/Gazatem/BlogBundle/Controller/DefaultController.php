<?php

namespace Gazatem\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('GazatemBlogBundle:Default:index.html.twig', array('name' => $name));
    }
    
    public function homeAction(){
        
        $em = $this->getDoctrine()->getEntityManager();

        $blog = $em->getRepository('GazatemBlogBundle:Blog')->findAll();

        if (!$blog) {
            throw $this->createNotFoundException('Unable to find Blog post.');
        }

        return $this->render('GazatemBlogBundle:Default:home.html.twig', array(
            'blogs'      => $blog,
        ));

    }
    
    
    public function repoAction(){
        
        $em = $this->getDoctrine()->getEntityManager();

       // $blog = $em->getRepository('GazatemBlogBundle:Blog')->findAll();

        
         $blog = $em->getRepository('GazatemBlogBundle:Blog')
                    ->getLatestBlogs();

        
        if (!$blog) {
            throw $this->createNotFoundException('Unable to find Blog post.');
        }

        return $this->render('GazatemBlogBundle:Default:home.html.twig', array(
            'blogs'      => $blog,
        ));

    }
    
}
